import Bun from 'bun';
import { watch } from 'fs/promises';
import process from 'process';

console.log('! Watching for changes');
const ac = new AbortController();
const watcher = watch('css', { recursive: true, signal: ac.signal });

// close watcher when Ctrl-C is pressed
process.on('SIGINT', () => {
	console.log('- Closing watcher...');
	ac.abort();
	process.exit(0);
});

const changes = new Set();
function compile() {
	for (const f of changes) {
		console.log('+ modified:', f);
	}
	changes.clear();
	Bun.spawn(['bun', 'css']);
}

let timer;

for await (const event of watcher) {
	// Debounce recompilation
	changes.add(event.filename);
	if (timer) clearTimeout(timer);
	timer = setTimeout(compile, 150);
}
