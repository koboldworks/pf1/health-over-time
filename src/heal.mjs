import { CFG } from './config.mjs';
import { setDefeated, initHoTData, isRelevantItem, sanitizeNum, signNum } from './common.mjs';
import { getSettings } from './utility.mjs';
import * as chat from './chat.mjs';
import * as data from './data/regen-data.mjs';
import { DocWrapper } from './doc-wrapper.mjs';
import { printRegenLog } from './debug.mjs';
import { Stabilization } from './data/regen-data.mjs';

class SanityCheckError extends Error { }

export function sanityCheck(updateData) {
	const numbers = ['system.attributes.hp.value', 'system.attributes.hp.nonlethal', 'system.attributes.hp.temp'];

	const foundNonNumber = numbers.some(k => k in updateData && typeof updateData[k] !== 'number');
	if (foundNonNumber) {
		const et = new Error(); // for adjusting line number
		console.error('ERROR', foundNonNumber, updateData[foundNonNumber]);
		throw new SanityCheckError('Health Over Time: Internal Error: Sanity check failed', undefined, et.lineNumber - 1);
	}
}

/**
 * @param {Actor} actor
 * @param {number} delta
 * @param {import("./data/state-data.mjs").HealthOverTimeState} d
 */
export const healTick = async (d) => {
	const actor = d.actor;
	if (actor.hasPlayerOwner) {
		// Process with player owner if they exist
		const owner = actor.activeOwner;
		if (owner && owner.id !== game.user.id)
			return;
	}

	const settings = getSettings();

	const combat = d.combat;

	if (combat) {
		const lastState = actor.getFlag(CFG.id, CFG.FLAGS.combatState);
		if (lastState?.id === combat.id && lastState?.round === combat.round) {
			console.warn(CFG.label, `| Already applied to ${actor.name} this round.`);
			d.status.nop = true;
			d.status.repeat = true;
			return;
		}
	}

	// Sanitize PF1 data
	d.past.value = sanitizeNum(d.past.value);
	d.past.temp = sanitizeNum(d.past.temp);
	d.past.nonlethal = sanitizeNum(d.past.nonlethal);

	const actorData = actor.system;

	const actorWrapper = new DocWrapper(actor);

	// --- Collect data ---
	try {
		if (settings.debug)
			console.log('Actor:', actor.name, actor.id, actor);

		d.past.bleeding = d.conditions.bleeding = d.itemFlags.bleeding;
		/*
		if (d.past.bleeding && settings.autoBleed)
			d.bleed.sources.push(new data.HealthChangeInstance('1', 'actor', actorWrapper));
		*/
		d.past.staggered = d.conditions.staggered = actor.statuses.has('staggered');

		d.regen.actor.parse(actorData.traits?.regen, settings, 'actor');
		if (d.regen.actor.value)
			d.regen.sources.push(new data.HealthChangeInstance(d.regen.actor.value, 'actor', actorWrapper));

		d.fastheal.actor.parse(actorData.traits?.fastHealing, settings, 'actor');
		if (d.fastheal.actor.value)
			d.fastheal.sources.push(new data.HealthChangeInstance(d.fastheal.actor.value, 'actor', actorWrapper));

		const addToHoTList = async (flag, iw) => {
			const item = iw.document;
			if (!item.hasItemBooleanFlag(flag)) return;

			const raw = item.getFlag(CFG.id, flag);
			const formula = raw || `${item.system.level || 0}`;

			const instance = new data.HealthChangeInstance(formula, item.id, iw);
			if (!instance.possiblyValid) return;

			const r = await instance.parse();
			if (r.isActive) return r;
		};

		// Parse sources
		const sourcePromises = [];
		for (const iw of d.items.regen) {
			const p = addToHoTList(CFG.FLAGS.regen, iw);
			p.then(r => r ? d.regen.sources.push(r) : null);
			sourcePromises.push(p);
		}
		for (const iw of d.items.fastHeal) {
			const p = addToHoTList(CFG.FLAGS.fastHeal, iw);
			p.then(r => r ? d.fastheal.sources.push(r) : null);
			sourcePromises.push(p);
		}
		for (const iw of d.items.bleed) {
			const p = addToHoTList(CFG.FLAGS.bleed, iw);
			p.then(r => r ? d.bleed.sources.push(r) : null);
			sourcePromises.push(p);
		}
		await Promise.all(sourcePromises);

		if (settings.debug)
			console.log('Initial state:', foundry.utils.deepClone(d));
	}
	finally {
		if (settings.debug)
			console.groupEnd();
	}

	// if (!token.actorLink) return; // don't handle unlinked tokens
	// TODO: Hook into options.advanceTime if it's ever used?
	// --- Process everything ---

	const msgs = [];

	// Dying condition treatment
	if (d.itemFlags.dying) {
		d.status.dying = true;

		if (settings.autoStabilize) {
			d.stabilization = new Stabilization(10 - d.post.value);

			// Stabilize
			await d.stabilization.stabilize(actor);

			if (settings.stabilizeCard && d.stabilization.roll)
				msgs.push(await chat.generateStabilizationCard(d, settings));
		}

		// Apply bleeding damage
		if (settings.autoBleed && !d.stabilization?.stabilized)
			d.bleed.sources.push(new data.HealthChangeInstance(1, 'actor'));

		// Disable dying buff if successfully stabilized
		if (settings.autoStabilize && d.stabilization.stabilized) {
			const dyingSources = actor.itemFlags.boolean.dying?.sources ?? [];
			for (const dying of dyingSources)
				dying.setActive(false);
		}
	}

	// --- Regeneration & Fast Healing ---
	// Configure
	const getBiggestHeal = (list) => list
		.reduce((total, current) => current.isActive ? Math.max(total, current.value) : total, -Infinity);

	d.regen.value.increase(sanitizeNum(getBiggestHeal(d.regen.sources)));
	d.fastheal.value.increase(sanitizeNum(getBiggestHeal(d.fastheal.sources)));
	d.bleed.value.increase(sanitizeNum(getBiggestHeal(d.bleed.sources)));

	d.status.effective = {
		regen: d.regen.value.isActive ? d.regen.value : null,
		fastheal: d.fastheal.value.isActive ? d.fastheal.value : null,
		bleed: d.bleed.value.isActive ? d.bleed.value : null,
	};

	// Apply
	// BLEED
	// Apply bleeding
	if (d.bleed.value.isActive) {
		let bleedRemaining = d.bleed.value.volume;
		// Bleed goes from temp HP first if available
		if (d.post.temp > 0) {
			d.post.temp -= bleedRemaining;
			bleedRemaining -= d.past.temp - d.post.temp;
			d.status.bleed.temp = d.past.temp - d.post.temp;
		}
		d.status.bleed.normal = bleedRemaining;
		d.post.value -= bleedRemaining;
	}

	// REGEN & FASTHEAL
	let leftoverHealing = d.regen.value.volume; // ADD IN REGEN

	d.past.dead = d.past.effective <= -actorData.abilities.con.total;

	// Check if auto-defeat conditions apply
	// BUG: This works only on bleed of 1
	if (settings.autoDefeat) {
		const newCurHP = d.post.effective + leftoverHealing;
		d.post.dead = newCurHP <= -actorData.abilities.con.total;
		d.diedNow = d.post.dead && d.past.dead !== d.post.dead;
	}

	// Check if character died from bleed
	if (d.post.dead && !d.diedNow && d.regen.value.volume <= 0) {
		if (settings.debug)
			console.log(CFG.label, '|', actor.name, `[${actor.id}]`, 'was found dead.');
		d.status.dead = true;
		// Should be marked defeated but isn't?
		d.status.nop = true;
		return; // They're kinda dead
	}

	// Add in fast healing
	// Fast healing does not work when dead
	if (!d.post.dead)
		leftoverHealing += d.fastheal.value.volume;

	const updateData = {};

	// Figure out if they have actually any fast healing or regeneration to do.
	if (leftoverHealing == 0 && !d.itemFlags.any && !d.stabilization?.roll) {
		if (settings.debug)
			console.log(CFG.label, '|', actor.name, `[${actor.id}]`, 'is a sad puppy and gets no cookie.');
		d.status.nop = true;
	}
	else {
		d.status.potential = leftoverHealing;

		// Apply regeneration
		// Nonlethal healing
		if (d.past.nonlethal > 0) {
			const v = d.post.nonlethal;
			d.post.nonlethal -= leftoverHealing;
			if (d.post.nonlethal < 0)
				d.post.nonlethal = 0;
			leftoverHealing -= d.past.nonlethal - v;
		}

		// Normal health healing
		if (d.post.value < d.past.max) {
			const v = d.post.value;
			d.post.value += leftoverHealing;
			if (d.post.value > d.post.max)
				d.post.value = d.post.max;
			leftoverHealing = d.post.value - v;
		}

		// --- Update Actor ---
		if (!d.post.valid) {
			console.error(CFG.label, '| SOMETHING WENT WRONG:', actor.name, d);
			d.status.error = true;
			return;
		}

		// Update HP
		if (d.post.nonlethal !== d.past.nonlethal)
			updateData['system.attributes.hp.nonlethal'] = d.post.nonlethal;
		if (d.post.value !== d.past.value)
			updateData['system.attributes.hp.value'] = d.post.value;
		if (d.post.temp !== d.past.temp)
			updateData['system.attributes.hp.temp'] = d.post.temp;

		sanityCheck(updateData);

		d.delta.value = d.post.value - d.past.value;
		d.delta.nonlethal = -(d.past.nonlethal - d.post.nonlethal);
		d.delta.temp = d.post.temp - d.past.temp;

		// setFlag replacement to avoid effective double update
		if (combat) {
			updateData[`flags.${CFG.id}.${CFG.FLAGS.combatState}`] = { id: combat.id, round: combat.round };
		}

		if (d.delta.value !== 0 || d.delta.temp !== 0 || d.delta.nonlethal !== 0) {
			d.status.awaken = d.past.effective < d.past.nonlethal && d.post.effective >= d.post.nonlethal;
			d.status.leftover = leftoverHealing;
			d.status.update = true;
		}
	}

	// TODO: Move output and updates out so it can be batched for long duration heals

	if (d.status.update) {
		d.updateData = updateData;
		await actor.update(updateData);

		// Print a chatcard about the changes
		if (settings.chatcard)
			msgs.push(await chat.generateCard(d, settings));

		printRegenLog(actor, d, settings);
	}

	if (d.diedNow) {
		// Bled out chat card
		if (settings.bleedoutCard)
			msgs.push(await chat.generateBleedoutCard(d, settings));

		// Automatic defeat
		if (settings.autoDefeat && !d.combatant.defeated)
			await setDefeated(d, true);
	}

	if (msgs.length)
		await ChatMessage.create(msgs);
};
