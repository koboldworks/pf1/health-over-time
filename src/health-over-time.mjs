import { CFG } from './config.mjs';
import * as chat from './chat.mjs';

import './item-config-ui.mjs';
import { onCombatUpdate } from './update-combat.mjs';
import { onSkippedTurns } from './skipped-turns.mjs';
import './modules/_init.mjs';
import './settings.mjs';
import * as migration from './migration/_module.mjs';

// TODO: Staggered condition at effective 0 HP.
// TODO: Unstagger at effective >0 HP.

CFG.cardTemplate = `modules/${CFG.id}/template/chatcard.hbs`;
CFG.stabilizationTemplate = `modules/${CFG.id}/template/stabilization.hbs`;

Hooks.on('renderChatMessage', chat.compactMessage);

Hooks.on('updateCombat', onCombatUpdate);
// Hooks.on('updateWorldTime', onTimePassing);

Hooks.on('pf1CombatTurnSkip', onSkippedTurns);

Hooks.once('ready', () => {
	game.modules.get(CFG.id).api = {
		migration: {
			item: migration.migrateItem,
			actor: migration.migrateActor,
			world: migration.migrateWorld,
		},
		debug: {
			time: false,
		},
	};
});
