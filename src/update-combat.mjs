import { CFG } from './config.mjs';
import { getSettings } from './utility.mjs';
import { healTick } from './heal.mjs';
import { HealthOverTimeState } from './data/state-data.mjs';

class SanityCheckError extends Error { }

export function sanityCheck(updateData) {
	const numbers = ['data.attributes.hp.value', 'data.attributes.hp.nonlethal', 'data.attributes.hp.temp'];
	const booleans = ['data.attributes.conditions.bleed'];

	const foundNonNumber = numbers.some(k => k in updateData && typeof updateData[k] !== 'number');
	const nB = booleans.some(k => k in updateData && typeof updateData[k] !== 'boolean');
	if (nB || foundNonNumber) {
		const et = new Error(); // for adjusting line number
		console.error('ERROR', nB, updateData[nB], foundNonNumber, updateData[foundNonNumber]);
		throw new SanityCheckError('Health Over Time: Internal Error: Sanity check failed', undefined, et.lineNumber - 1);
	}
}

/**
 * @param {Combat} combat
 * @param {object} changed
 * @param {object} _options
 * @param {string} userId
 */
export async function onCombatUpdate(combat, changed, _options, userId) {
	if (!combat.started) return;

	/** @type {Combatant} */
	const combatant = combat.combatant,
		actor = combatant.actor;

	// Combatant has no actor or current user is not its owner
	if (!actor?.isOwner) return;

	// Skip unsupported actor types
	if (!['character', 'npc'].includes(actor.type)) return;

	const defeated = combatant?.isDefeated ?? false;
	// Don't process tokens that are defeated.
	if (defeated) return;

	// Figure out who should update
	const owner = actor.activeOwner;
	if (owner) {
		// Has active owner, but it's not us
		if (!owner.isSelf) return;
	}
	else {
		throw new Error('No owner detected!');
	}

	const settings = getSettings();
	if (settings.debug) {
		console.group(CFG.label);
		console.log('Combat round:', changed.round, 'Turn:', combat.turn, 'ID:', combat.id);
		console.log('Settings:', settings);
	}

	const lastState = actor.getFlag(CFG.id, CFG.FLAGS.combatState);
	if (lastState?.id === combat.id && lastState?.round === combat.round) {
		console.warn(CFG.label, `| Already applied to ${actor.name} this round.`);
		if (settings.debug) console.groupEnd();
		return;
	}

	const state = new HealthOverTimeState(actor, { combat, token: combatant.token, time: CONFIG.time.roundTime });

	const rv = healTick(state);

	// console.log({ result: await rv });
}
