import { CFG } from './config.mjs';

/**
 * @returns {import('./data/regen-config.mjs').RegenConfigData}
 */
export function getSettings() {
	const cfg = game.settings.get(CFG.id, 'config');
	// Shim to simplify things and "override" the above datamodel
	cfg.chatcard = game.settings.get(CFG.id, 'chatcard');
	cfg.transparency = game.settings.get(CFG.id, 'transparency');
	cfg.autoBleed = game.settings.get(CFG.id, 'autoBleed');
	cfg.autoDefeat = game.settings.get(CFG.id, 'autoDefeat');
	cfg.autoStabilize = game.settings.get(CFG.id, 'autoStabilize');
	cfg.stablizeCard = game.settings.get(CFG.id, 'stabilizeCard');
	cfg.debug = game.settings.get(CFG.id, 'debug');

	try {
		cfg.ignoreRE = cfg.ignore.reduce((arr, re) => { arr.push(new RegExp(re, 'i')); return arr; }, []);
	}
	catch (err) {
		console.error('Health Over Time | Failure to compile regular expressions:\n', cfg.ignore);
		ui.notifications.error('Failure to compile regular expressions for regeneration.');
		cfg.ignoreRE = [];
	}

	return cfg;
}
