import { CFG } from './config.mjs';
import { RegenConfigData } from './data/regen-config.mjs';
import { IgnorePatternConfig } from './ignore-pattern-config.mjs';

const migrateSettings = async () => {
	const s = game.settings.get(CFG.id, 'config');
	if (s.migrated == 0) {
		game.settings.set(CFG.id, 'chatcard', s.chatcard);
		game.settings.set(CFG.id, 'transparency', s.transparency);
		game.settings.set(CFG.id, 'autoBleed', s.autoBleed);
		game.settings.set(CFG.id, 'autoDefeat', s.autoDefeat);
		game.settings.set(CFG.id, 'autoStabilize', s.autoStabilize);
		game.settings.set(CFG.id, 'stabilizeCard', s.stabilizeCard);

		const ms = s.toObject();
		ms.migrated = 1;
		game.settings.set(CFG.id, 'config', ms);
		ui.notifications.info('Health Over Time Configuration Migrated');
	}
	else {
		console.log('Health Over Time Configuration already migrated');
	}
};

Hooks.once('init', () => {
	game.settings.register(CFG.id, 'config', {
		scope: 'world',
		type: RegenConfigData,
		config: false,
		default: new RegenConfigData(),
	});

	game.settings.register(CFG.id, 'chatcard', {
		name: 'HealthOverTime.Config.ChatCardLabel',
		hint: 'HealthOverTime.Config.ChatCardHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'transparency', {
		name: 'HealthOverTime.Config.TransparencyLabel',
		hint: 'HealthOverTime.Config.TransparencyHint',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'autoBleed', {
		name: 'HealthOverTime.Config.AutoBleedLabel',
		hint: 'HealthOverTime.Config.AutoBleedHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'autoDefeat', {
		name: 'HealthOverTime.Config.AutoDefeatLabel',
		hint: 'HealthOverTime.Config.AutoDefeatHint',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'autoStabilize', {
		name: 'HealthOverTime.Config.AutoStabilizeLabel',
		hint: 'HealthOverTime.Config.AutoStabilizeHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'stabilizeCard', {
		name: 'HealthOverTime.Config.StabilizeCardLabel',
		hint: 'HealthOverTime.Config.StabilizeCardHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.registerMenu(CFG.id, 'config', {
		id: 'pf1-health-over-time-config',
		label: 'HealthOverTime.Config.IgnorePatternButton',
		name: 'HealthOverTime.Config.IgnorePatternTitle',
		hint: 'HealthOverTime.Config.IgnorePatternHint',
		scope: 'world',
		icon: 'fas fa-tint',
		type: IgnorePatternConfig,
		config: true,
		restricted: true,
	});

	game.settings.register(CFG.id, 'debug', {
		name: 'HealthOverTime.Config.DebugLabel',
		hint: 'HealthOverTime.Config.DebugHint',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'migration', {
		type: String,
		default: '0.6.0',
		scope: 'world',
		config: false,
	});
});

Hooks.once('ready', () => {
	if (game.user.activeGM?.isSelf)
		migrateSettings();
});
