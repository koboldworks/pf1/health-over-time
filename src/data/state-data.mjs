import { CFG } from '../config.mjs';
import { isRelevantItem } from '../common.mjs';
import * as data from './regen-data.mjs';
import { DocWrapper } from '../doc-wrapper.mjs';

export class HealthOverTimeState {
	/** @type {number} */
	time;

	card = {
		css: CFG.id,
		// round: game.i18n.format('HealthOverTime.Round', { round: combat.round }),
	};

	/** @type {Combat} */
	combat;

	/** @type {Combatant} */
	combatant;

	/** @type {Actor} */
	actor;

	/** @type {Token} */
	token;

	fastheal = {
		value: new data.HealthChange,
		actor: new data.HoTSource,
		sources: [],
	};

	regen = {
		value: new data.HealthChange,
		actor: new data.HoTSource,
		sources: [],
	};

	bleed = {
		value: new data.HealthChange,
		sources: [],
	};

	itemFlags = {
		get any() {
			return this.bleeding || this.regen || this.dying || this.fastHeal;
		},
		bleeding: false,
		regen: false,
		fastHeal: false,
		dying: false
	};

	dead = false; // Dead in general
	diedNow = false; // Died on this pass

	past = new data.HealthState();
	post = new data.HealthState();
	delta = new data.HealthDelta();
	conditions = new data.ConditionState();

	stabilization = null;

	debug = {};

	status = {
		update: false, // Update happened
		nop: false, // Nothing was done
		repeat: false, // Repeat attempt to heal
		potential: 0, // Potential change
		leftover: 0, // Leftover healing
		awaken: undefined, // Did the character awaken?
		dead: false, // Is dead
		bleed: { temp: 0, normal: 0 }, // Where bleeding was applied to
	};

	items = {};

	constructor(actor, { combat, token, time } = {}) {
		this.actor = actor;
		this.combat = combat;
		this.combatant = combat?.combatant;
		this.token = token ?? this.combatant?.token?.object ?? this.actor.token?.object;
		this.time = time;

		const actorData = actor.system;
		this.past = new data.HealthState(actorData.attributes.hp);
		this.post = new data.HealthState(actorData.attributes.hp);

		this.itemFlags.bleeding = actor.hasItemBooleanFlag(CFG.FLAGS.bleed);
		this.itemFlags.regen = actor.hasItemBooleanFlag(CFG.FLAGS.regen);
		this.itemFlags.fastHeal = actor.hasItemBooleanFlag(CFG.FLAGS.fastHeal);
		this.itemFlags.dying = actor.hasItemBooleanFlag(CFG.FLAGS.dying);

		// TODO: Cache this result somehow until actor is updated?
		for (const flag of [CFG.FLAGS.regen, CFG.FLAGS.bleed, CFG.FLAGS.fastHeal, CFG.FLAGS.dying]) {
			this.items[flag] = new Collection();
			const items = this.items[flag];
			const flagged = actor.itemFlags.boolean[flag]?.sources ?? [];
			for (const item of flagged) {
				if (!item.isActive) continue;
				items.set(item.id, new DocWrapper(item));
			}
		}
	}
}
