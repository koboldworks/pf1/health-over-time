import { CFG } from './config.mjs';
import { HealthOverTimeState } from './data/state-data.mjs';

export const signNum = new Intl.NumberFormat(undefined, { signDisplay: 'always' }).format;

/**
 * @param {Actor|Item} thing
 * @param {any} permission
 * @returns {User[]}
 */
export const getUsers = (thing, permission) => Object
	.keys(thing.ownership)
	.map(uid => game.users.get(uid))
	.filter(user => user ? thing.testUserPermission(user, permission) : false);

/**
 * @param {Actor} actor
 */
export const getActiveOwners = (actor) => getUsers(actor, CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER)
	.filter(u => u.active && !u.isGM);

export const setDefeated = async (d, defeated) => {
	try {
		await d.combat.updateEmbeddedDocuments('Combatant', [{ _id: d.combatant.id, defeated }]);
		const status = CONFIG.statusEffects.find(ae => ae.id === CONFIG.specialStatusEffects.DEFEATED);
		const effect = d.token.actor && status ? status : CONFIG.controlIcons.defeated;
		return d.token.toggleEffect(effect, { active: true, overlay: true });
	}
	catch (err) {
		console.error('Error marking combatant defeated:', err);
	}
};

/**
 * TODO: Turn this into a class.
 *
 * @param {Actor} actor
 * @param {object} [options]
 * @param {Combat} [options.combat]
 * @param {number} [options.time]
 */
export const initHoTData = (actor, { combat, time } = {}) => new HealthOverTimeState(actor, { combat, time });

/**
 * Tests if an item is relevant to this module's functionality.
 *
 * @param {Item} item
 * @returns {boolean}
 */
export const isRelevantItem = (item) => item.getItemBooleanFlags().some(f => [CFG.FLAGS.regen, CFG.FLAGS.bleed, CFG.FLAGS.fastHeal, CFG.FLAGS.dying].includes(f));

/**
 * Make sure number is actually a number.
 *
 * @param {any} num
 * @returns {number}
 */
export const sanitizeNum = (num) => Number.isFinite(num) ? num : 0;
