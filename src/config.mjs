export const CFG = /** @type {const} */ ({
	label: '+- Health over Time',
	id: 'pf1-health-over-time',
	SETTINGS: {

	},
	FLAGS: {
		combatState: 'combatState',
		tick: 'tick',
		bleed: 'bleed',
		regen: 'regen',
		fastHeal: 'fastHeal',
		dying: 'dying',
	},
	cardTemplate: undefined,
	stabilizationTemplate: undefined,
});
